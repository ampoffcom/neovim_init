let g:python_host_prog='/usr/bin/python2'
set shell=zsh

filetype off
set rtp+=~/.vim/bundle/Vundle.vim/
call vundle#begin()

Plugin 'gmarik/vundle'
Plugin 'CSApprox'
Plugin 'scrooloose/nerdtree.git'
Plugin 'https://github.com/Valloric/YouCompleteMe.git', { 'do': './install.sh' }
Plugin 'Darcs'
Plugin 'xml.vim'
Plugin 'bufexplorer.zip'
Plugin 'rbgrouleff/bclose.vim'
Plugin 'https://github.com/davidhalter/jedi'
Plugin 'https://github.com/scrooloose/syntastic'
Plugin 'https://github.com/Twinside/vim-haskellConceal'
Plugin 'https://github.com/bling/vim-airline'
Plugin 'https://github.com/farseer90718/vim-taskwarrior'
Plugin 'godlygeek/tabular'
Plugin 'plasticboy/vim-markdown'
Plugin 'lervag/vimtex'
Plugin 'https://github.com/gosukiwi/vim-atom-dark/'
Plugin 'https://github.com/ludovicchabant/vim-lawrencium'
Plugin 'fatih/vim-go'
Plugin 'junegunn/goyo.vim'
Plugin 'junegunn/limelight.vim'
Plugin 'junegunn/seoul256.vim'
Plugin 'wannesm/wmgraphviz.vim'
Plugin 'https://github.com/vimwiki/vimwiki'
Plugin 'airblade/vim-gitgutter'
Plugin 'https://github.com/mhinz/vim-startify'
call vundle#end()

filetype plugin indent on

set textwidth=120
set tabstop=4
set expandtab
set softtabstop=4
set shiftwidth=4
set shiftround

set encoding=utf-8
set showmode
set showcmd
set number
"set foldmethod=manual
set foldmethod=marker
"set relativenumber

syntax on

nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>

tnoremap <Esc> <C-\><C-n>

set list
set listchars=tab:>.,trail:.,extends:#,nbsp:.
"autocmd filetype html,xml set listchars-=tab:>.
"autocmd VimEnter * Limelight .

"Neovim-qt Guifont command, to change the font
command -nargs=? Guifont call rpcnotify(0, 'Gui', 'SetFont', "<args>") | let g:Guifont="<args>"
Guifont DejaVu Sans Mono:h14

set pastetoggle=<F2>
set guifont=monoisome\ 13

"airline
set laststatus=2

"NERDTree
map <F2> :NERDTreeToggle<CR>

"colo seoul256
"colo seoul256-light
"colorscheme atom-dark
colorscheme slate
"colorscheme torte
"
"let g:syntastic_python_python_exec = '/usr/bin/python2'
let g:syntastic_python_python_exec = '/usr/bin/python3'

au BufNewFile,BufRead,BufEnter *_de.txt setlocal spell spelllang=de_de
au BufNewFile,BufRead,BufEnter *_en.txt setlocal spell spelllang=en_us
au BufNewFile,BufRead,BufEnter *_de.md setlocal spell spelllang=de_de
au BufNewFile,BufRead,BufEnter *_en.md setlocal spell spelllang=en_us

nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>
